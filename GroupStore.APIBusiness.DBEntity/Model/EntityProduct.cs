﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityProduct
    {

        public int idProducto { get; set; }
        public string codigoProducto { get; set; }
        public string nombreProducto { get; set; }
        public int idCategoria { get; set; }
        public string descripcion { get; set; }
        public double precioBase { get; set; }
        public int cantidad { get; set; }
        public int stockMinimo { get; set; }
        public string imageFile { get; set; }
    }
}
