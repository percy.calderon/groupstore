﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DBEntity
{
    public class EntityUser : EntityBase
    {
        public int idPersona { get; set; } // MEJORA: Reemplazar por Key, ID - Alias BD
        public string dni { get; set; }
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public string sexo { get; set; }
        public DateTime nacimiento { get; set; }
        public string email { get; set; }
        public string celular { get; set; }
        public string idDistrito { get; set; }
        public string direccion { get; set; }
        public string referencia { get; set; }
        public string contraseña { get; set; }
        public string role { get; set; }

    }
}
