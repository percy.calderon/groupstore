﻿using DBEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBContext
{
    public interface IUserRepository
    {
        List<EntityUser> GetUsers(int idPersona, int tipoBusqueda);
        EntityUser SearchUser(string email, string password);
        bool InsertUser(EntityUser oUser);
        bool UpdatetUser(EntityUser oUser);
    }
}
