﻿using DBEntity;
using System;
using System.Collections.Generic;
using System.Text;

namespace DBContext
{
    public interface IStockRepository
    {
        ResponseBase GetStock();
        ResponseBase GetStock(EntityStockByCategory stockByCategory);
        ResponseBase GetStock(EntityStockByProduct stockByProduct);
    }
}
