﻿using Dapper;
using DBEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DBContext
{
    public class ProductRepository : BaseRepository, IProductRepository
    {
        public List<EntityProduct> GetProducts()
        {
            var returnEntity = new List<EntityProduct>();
            try
            {
                using (var db = GetSqlConnection())
                {
                    const string sql = @"usp_Obtener_Productos";

                    returnEntity = db.Query<EntityProduct>(sql,
                        commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return returnEntity;
        }

    }
}
