﻿using Dapper;
using DBEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DBContext
{
    public class RegistroVentaRepository: BaseRepository, IRegistroVentaRepository
    {
        public ResponseBase SetRegistroVenta(EntityRegistroVenta registroVenta)
        {
            var returnEntity = new ResponseBase();
            var registroVentaResponse = new EntityRegistroVenta();
        try
            {
                using (var db = GetSqlConnection())
                {
                    const string sql = @"usp_Venta_Insert";
                    var a = new DynamicParameters();
                    a.Add(name: "@idTipoComp", value: registroVenta.IdTipoComp, DbType.Int32, direction: ParameterDirection.Input);
                    a.Add(name: "@idCliente", value: registroVenta.IdCliente, DbType.Int32, direction: ParameterDirection.Input);
                    a.Add(name: "@idVendedor", value: registroVenta.IdVendedor, DbType.Int32, direction: ParameterDirection.Input);
                    a.Add(name: "@idDespacho", value: registroVenta.IdDepacho, DbType.Int32, direction: ParameterDirection.Input);
                    a.Add(name: "@idVenta", value: registroVenta.IdVenta, DbType.Int64, direction: ParameterDirection.Input);
                    a.Add(name: "@idProducto", value: registroVenta.IdProducto, DbType.Int32, direction: ParameterDirection.Input);
                    a.Add(name: "@cantidad", value: registroVenta.Cantidad, DbType.Decimal, direction: ParameterDirection.Input);
                    a.Add(name: "@error", value: registroVenta.error, DbType.String, direction: ParameterDirection.Input);
                    registroVentaResponse = db.Query<EntityRegistroVenta>(sql, param: a, commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            if (registroVentaResponse != null)
                {
                    returnEntity.issuccess = true;
                    returnEntity.errocode = "00000";
                    returnEntity.erromessage = string.Empty;
                    returnEntity.Data = registroVentaResponse;
                }
            else
                {
                    returnEntity.issuccess = false;
                    returnEntity.errocode = "00000";
                    returnEntity.erromessage = string.Empty;
                    returnEntity.Data = null;
                }
            }
            catch (Exception ex)
            {
                returnEntity.issuccess = false;
                returnEntity.errocode = "00000";
                returnEntity.erromessage = ex.Message;
                returnEntity.Data = null;
            }

            return returnEntity;
        }
    }
}
