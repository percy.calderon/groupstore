﻿using Dapper;
using DBEntity;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace DBContext
{
    public class UserRepository : BaseRepository, IUserRepository
    {
        public List<EntityUser> GetUsers(int idPersona, int tipoBusqueda)
        {
            var returnEntity = new List<EntityUser>();

            try
            {
                using (var db = GetSqlConnection())
                {
                    const string sql = @"usp_Listar_Personas";
                    var p = new DynamicParameters();
                    p.Add(name: "@idPersona", value: idPersona,
                        dbType: DbType.Int32, direction: ParameterDirection.Input);
                    p.Add(name: "@tipoBusqueda", value: tipoBusqueda,
                        dbType: DbType.Int32, direction: ParameterDirection.Input);

                    returnEntity = db.Query<EntityUser>(sql,
                        commandType: CommandType.StoredProcedure).ToList();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
          
            return returnEntity;
        }
        public EntityUser SearchUser(string email, string password)
        {
            var returnEntity = new EntityUser();
            try
            {
                using (var db = GetSqlConnection())
                {
                    const string sql = @"usp_Persona_Login";
                    var p = new DynamicParameters();
                    p.Add(name: "@email", value: email,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@contraseña", value: password,
                        dbType: DbType.String, direction: ParameterDirection.Input);

                    returnEntity = db.Query<EntityUser>(sql,
                        commandType: CommandType.StoredProcedure).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            
            return returnEntity;
        }

        public bool InsertUser(EntityUser oUser)
        {
            bool result = false ;
            try
            {
                using (var db = GetSqlConnection())
                {
                    const string sql = @"usp_Insertar_Persona";
                    var p = new DynamicParameters();
                    p.Add(name: "@dni", value: oUser.dni,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@nombres", value: oUser.nombres,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@apellidos", value: oUser.apellidos,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@sexo", value: oUser.sexo,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@nacimiento", value: oUser.nacimiento,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@email", value: oUser.email,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@celular", value: oUser.celular,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@idDistrito", value: oUser.idDistrito,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@direccion", value: oUser.direccion,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@referencia", value: oUser.referencia,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@contraseña", value: oUser.contraseña,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@role", value: oUser.role,
                        dbType: DbType.String, direction: ParameterDirection.Input);

                    result = db.Query<int>(sql,
                        commandType: CommandType.StoredProcedure).Any();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

            return result;
        }

        public bool UpdatetUser(EntityUser oUser)
        {
            bool result = false;
            try
            {
                using (var db = GetSqlConnection())
                {
                    const string sql = @"usp_Insertar_Persona";
                    var p = new DynamicParameters();
                    p.Add(name: "@dni", value: oUser.dni,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@nombres", value: oUser.nombres,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@apellidos", value: oUser.apellidos,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@sexo", value: oUser.sexo,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@nacimiento", value: oUser.nacimiento,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@email", value: oUser.email,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@celular", value: oUser.celular,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@idDistrito", value: oUser.idDistrito,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@direccion", value: oUser.direccion,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@referencia", value: oUser.referencia,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@contraseña", value: oUser.contraseña,
                        dbType: DbType.String, direction: ParameterDirection.Input);
                    p.Add(name: "@role", value: oUser.role,
                        dbType: DbType.String, direction: ParameterDirection.Input);

                    result = db.Query<int>(sql,
                        commandType: CommandType.StoredProcedure).Any();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }

            return result;
        }




    }
}
