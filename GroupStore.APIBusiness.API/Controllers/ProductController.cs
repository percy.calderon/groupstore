﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBContext;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GroupStore.APIBusiness.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : Controller
    {
        protected readonly IProductRepository _ProductRepository;

        public ProductController(IProductRepository productRepository)
        {
            _ProductRepository = productRepository;
        }

      
        [Produces("application/json")]
        [AllowAnonymous] // Despues Autenticado... con token
        [HttpGet]
        [Route("GetProjects")]//si queires otro nombre
        public ActionResult GetProject()
        {
            var ret = _ProductRepository.GetProducts();
            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }
    }
}
