﻿using DBContext;
using DBEntity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NSwag.Annotations;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;

namespace UPC.APIBusiness.API.Controllers
{
        [Produces("application/json")]
        [Route("api/login")]
        [ApiController]
        public class RegistroVentaController : Controller
        {
            /// <summary>
            /// 
            /// </summary>
            protected readonly IRegistroVentaRepository _RegistroVentaRepository;
            /// <summary>
            /// 
            /// </summary>
            /// <param name="RegistroVentaRepository"></param>
            public RegistroVentaController(IRegistroVentaRepository RegistroVentaRepository)
            {
                _RegistroVentaRepository = RegistroVentaRepository;
            }
            /// <summary>
            /// 
            /// </summary>
            /// <param name="registroVenta"></param>
            /// <returns></returns>
            [Produces ("application/json")]
            [AllowAnonymous]
            [HttpPost]
            [Route("RegistroVenta")]
            public ActionResult RegistroVenta(EntityRegistroVenta registroVenta)
            {
                var ret = _RegistroVentaRepository.SetRegistroVenta(registroVenta);
                if (ret == null)
                    return StatusCode(401);
                return Json(ret);
            }
        }
    }

