﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DBContext;
using DBEntity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace GroupStore.APIBusiness.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        protected readonly IUserRepository _UserRepository;

        public UserController(IUserRepository userRepository)
        {
            _UserRepository = userRepository;
        }

        [Produces("application/json")]
        [AllowAnonymous] // Despues Autenticado... con token
        [HttpGet]
        [Route("SearchUsers")]//si queires otro nombre
        public ActionResult SearchUsers(int idPersona, int tipoBusqueda)
        {
            var ret = _UserRepository.GetUsers(idPersona, tipoBusqueda);
            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }

        [Produces("application/json")]
        [AllowAnonymous] // Despues Autenticado... con token
        [HttpGet]
        [Route("Login")]
        public ActionResult Login(string email, string password)
        {
            var ret = _UserRepository.SearchUser(email, password);
            if (ret == null)
                return StatusCode(401);

            return Json(ret);
        }

        [Produces("application/json")]
        [AllowAnonymous] // Despues Autenticado... con token
        [HttpGet]
        [Route("InsertUser")]//si queires otro nombre
        public ActionResult InsertUser(EntityUser oUser)
        {
            var ret = _UserRepository.InsertUser(oUser);
            if (!ret)
                return StatusCode(401);

            return Json(ret);
        }


        [Produces("application/json")]
        [AllowAnonymous]
        [HttpGet]
        [Route("UpdatetUser")]
        public ActionResult UpdatetUser(EntityUser oUser)
        {
            var ret = _UserRepository.UpdatetUser(oUser);
            if (!ret)
                return StatusCode(401);

            return Json(ret);
        }

        //List<EntityUser> GetUsers(int idPersona, int tipoBusqueda);
        //EntityUser SearchUser(string email, string password);
        //bool InsertUser(EntityUser oUser);
        //bool UpdatetUser(EntityUser oUser);
    }
}
